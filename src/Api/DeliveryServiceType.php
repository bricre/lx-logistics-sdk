<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\DeliveryServiceType\Read;

class DeliveryServiceType extends AbstractAPI
{
    /**
     * Retrieves the collection of DeliveryServiceType resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'order[code]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getDeliveryServiceTypeCollection',
        'GET',
        'api/logistics/delivery_service_types',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a DeliveryServiceType resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getDeliveryServiceTypeItem',
        'GET',
        "api/logistics/delivery_service_types/$code",
        null,
        [],
        []
        );
    }
}
