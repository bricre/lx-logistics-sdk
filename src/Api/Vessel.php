<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\Vessel\Read;
use LogisticsX\Logistics\Model\Vessel\Write;

class Vessel extends AbstractAPI
{
    /**
     * Retrieves the collection of Vessel resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'provider.code'	string
     *                       'provider.name'	string
     *                       'provider.shippingType'	string
     *                       'order[code]'	string
     *                       'order[provider.code]'	string
     *                       'order[provider.name]'	string
     *                       'order[provider.shippingType]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getVesselCollection',
        'GET',
        'api/logistics/vessels',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Vessel resource.
     *
     * @param Write $Model The new Vessel resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postVesselCollection',
        'POST',
        'api/logistics/vessels',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Vessel resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getVesselItem',
        'GET',
        "api/logistics/vessels/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Vessel resource.
     *
     * @param string $code  Resource identifier
     * @param Write  $Model The updated Vessel resource
     *
     * @return Read
     */
    public function putItem(string $code, Write $Model): Read
    {
        return $this->request(
        'putVesselItem',
        'PUT',
        "api/logistics/vessels/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Vessel resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteVesselItem',
        'DELETE',
        "api/logistics/vessels/$code",
        null,
        [],
        []
        );
    }
}
