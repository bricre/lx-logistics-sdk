<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\Location\Read;
use LogisticsX\Logistics\Model\Location\Write;

class Location extends AbstractAPI
{
    /**
     * Retrieves the collection of Location resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'address'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[type]'	string
     *                       'order[address]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getLocationCollection',
        'GET',
        'api/logistics/locations',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Location resource.
     *
     * @param Write $Model The new Location resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postLocationCollection',
        'POST',
        'api/logistics/locations',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Location resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getLocationItem',
        'GET',
        "api/logistics/locations/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Location resource.
     *
     * @param string $code  Resource identifier
     * @param Write  $Model The updated Location resource
     *
     * @return Read
     */
    public function putItem(string $code, Write $Model): Read
    {
        return $this->request(
        'putLocationItem',
        'PUT',
        "api/logistics/locations/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Location resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteLocationItem',
        'DELETE',
        "api/logistics/locations/$code",
        null,
        [],
        []
        );
    }
}
