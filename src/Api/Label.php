<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\ConsignmentInfo;
use LogisticsX\Logistics\Model\ConsignmentUUIDS;
use LogisticsX\Logistics\Model\LabelResult;
use LogisticsX\Logistics\Model\Result;

class Label extends AbstractAPI
{
    /**
     * Cancel label for consignment.
     *
     * @param ConsignmentUUIDS $Model
     *
     * @return Result
     */
    public function cancel(ConsignmentUUIDS $Model): Result
    {
        return $this->request(
        'cancelLabel',
        'POST',
        'api/logistics/cancel_label',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * get label for consignment.
     *
     * @param ConsignmentInfo $Model
     *
     * @return LabelResult
     */
    public function get(ConsignmentInfo $Model): LabelResult
    {
        return $this->request(
        'getLabel',
        'POST',
        'api/logistics/get_label',
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
