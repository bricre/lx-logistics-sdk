<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\DeliveryService\DeliveryServiceRead;
use LogisticsX\Logistics\Model\DeliveryService\DeliveryServiceWrite;
use LogisticsX\Logistics\Model\DeliveryService\Statistics;
use LogisticsX\Logistics\Model\Order;
use LogisticsX\Logistics\Model\Result;

class DeliveryService extends AbstractAPI
{
    /**
     * Check order info by service limitation formula.
     *
     * @param Order $Model
     *
     * @return Result
     */
    public function checkLimitations(Order $Model): Result
    {
        return $this->request(
        'checkLimitations',
        'POST',
        'api/logistics/check_limitations',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of DeliveryService resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'deliveryServiceType.code'	string
     *                       'deliveryServiceType.type'	string
     *                       'deliveryServiceAccount.code'	string
     *                       'deliveryServiceAccount.name'	string
     *                       'config'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[deliveryServiceType.code]'	string
     *                       'order[deliveryServiceAccount.code]'	string
     *                       'order[deliveryServiceAccount.name]'	string
     *                       'order[config]'	string
     *                       'order[status]'	string
     *
     * @return DeliveryServiceRead[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getDeliveryServiceCollection',
        'GET',
        'api/logistics/delivery_services',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a DeliveryService resource.
     *
     * @param DeliveryServiceWrite $Model The new DeliveryService resource
     *
     * @return DeliveryServiceRead
     */
    public function postCollection(DeliveryServiceWrite $Model): DeliveryServiceRead
    {
        return $this->request(
        'postDeliveryServiceCollection',
        'POST',
        'api/logistics/delivery_services',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of DeliveryService resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'deliveryServiceType.code'	string
     *                       'deliveryServiceType.type'	string
     *                       'deliveryServiceAccount.code'	string
     *                       'deliveryServiceAccount.name'	string
     *                       'config'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[deliveryServiceType.code]'	string
     *                       'order[deliveryServiceAccount.code]'	string
     *                       'order[deliveryServiceAccount.name]'	string
     *                       'order[config]'	string
     *                       'order[status]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsDeliveryServiceCollection',
        'GET',
        'api/logistics/delivery_services/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a DeliveryService resource.
     *
     * @param string $id Resource identifier
     *
     * @return DeliveryServiceRead|null
     */
    public function getItem(string $id): ?DeliveryServiceRead
    {
        return $this->request(
        'getDeliveryServiceItem',
        'GET',
        "api/logistics/delivery_services/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the DeliveryService resource.
     *
     * @param string               $id    Resource identifier
     * @param DeliveryServiceWrite $Model The updated DeliveryService resource
     *
     * @return DeliveryServiceRead
     */
    public function putItem(string $id, DeliveryServiceWrite $Model): DeliveryServiceRead
    {
        return $this->request(
        'putDeliveryServiceItem',
        'PUT',
        "api/logistics/delivery_services/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the DeliveryService resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteDeliveryServiceItem',
        'DELETE',
        "api/logistics/delivery_services/$id",
        null,
        [],
        []
        );
    }
}
