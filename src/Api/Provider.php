<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\Provider\Read;
use LogisticsX\Logistics\Model\Provider\Statistics;
use LogisticsX\Logistics\Model\Provider\Write;

class Provider extends AbstractAPI
{
    /**
     * Retrieves the collection of Provider resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'shippingType'	string
     *                       'shippingType[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[status]'	string
     *                       'order[shippingType]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getProviderCollection',
        'GET',
        'api/logistics/providers',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Provider resource.
     *
     * @param Write $Model The new Provider resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postProviderCollection',
        'POST',
        'api/logistics/providers',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Provider resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'shippingType'	string
     *                       'shippingType[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[status]'	string
     *                       'order[shippingType]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsProviderCollection',
        'GET',
        'api/logistics/providers/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Provider resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getProviderItem',
        'GET',
        "api/logistics/providers/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Provider resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated Provider resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putProviderItem',
        'PUT',
        "api/logistics/providers/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Provider resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteProviderItem',
        'DELETE',
        "api/logistics/providers/$id",
        null,
        [],
        []
        );
    }
}
