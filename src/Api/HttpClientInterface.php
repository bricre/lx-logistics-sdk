<?php

namespace LogisticsX\Logistics\Api;

use Psr\Http\Client\ClientInterface as BaseClass;

interface HttpClientInterface extends BaseClass
{
}
