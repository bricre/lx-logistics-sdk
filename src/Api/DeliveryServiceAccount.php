<?php

namespace LogisticsX\Logistics\Api;

use LogisticsX\Logistics\Model\DeliveryServiceAccount\DeliveryServiceAccountRead;
use LogisticsX\Logistics\Model\DeliveryServiceAccount\DeliveryServiceAccountWrite;
use LogisticsX\Logistics\Model\DeliveryServiceAccount\Statistics;

class DeliveryServiceAccount extends AbstractAPI
{
    /**
     * Retrieves the collection of DeliveryServiceAccount resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'deliveryServiceType.code'	string
     *                       'config'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[deliveryServiceType.code]'	string
     *                       'order[config]'	string
     *                       'order[status]'	string
     *
     * @return DeliveryServiceAccountRead[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getDeliveryServiceAccountCollection',
        'GET',
        'api/logistics/delivery_service_accounts',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a DeliveryServiceAccount resource.
     *
     * @param DeliveryServiceAccountWrite $Model The new DeliveryServiceAccount
     *                                           resource
     *
     * @return DeliveryServiceAccountRead
     */
    public function postCollection(DeliveryServiceAccountWrite $Model): DeliveryServiceAccountRead
    {
        return $this->request(
        'postDeliveryServiceAccountCollection',
        'POST',
        'api/logistics/delivery_service_accounts',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of DeliveryServiceAccount resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'deliveryServiceType.code'	string
     *                       'config'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[deliveryServiceType.code]'	string
     *                       'order[config]'	string
     *                       'order[status]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsDeliveryServiceAccountCollection',
        'GET',
        'api/logistics/delivery_service_accounts/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a DeliveryServiceAccount resource.
     *
     * @param string $id Resource identifier
     *
     * @return DeliveryServiceAccountRead|null
     */
    public function getItem(string $id): ?DeliveryServiceAccountRead
    {
        return $this->request(
        'getDeliveryServiceAccountItem',
        'GET',
        "api/logistics/delivery_service_accounts/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the DeliveryServiceAccount resource.
     *
     * @param string                      $id    Resource identifier
     * @param DeliveryServiceAccountWrite $Model The updated DeliveryServiceAccount
     *                                           resource
     *
     * @return DeliveryServiceAccountRead
     */
    public function putItem(string $id, DeliveryServiceAccountWrite $Model): DeliveryServiceAccountRead
    {
        return $this->request(
        'putDeliveryServiceAccountItem',
        'PUT',
        "api/logistics/delivery_service_accounts/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the DeliveryServiceAccount resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteDeliveryServiceAccountItem',
        'DELETE',
        "api/logistics/delivery_service_accounts/$id",
        null,
        [],
        []
        );
    }
}
