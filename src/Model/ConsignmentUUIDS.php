<?php

namespace LogisticsX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentUUIDS extends AbstractModel
{
    /**
     * @var string[]
     */
    public $consignmentUUIDs = null;
}
