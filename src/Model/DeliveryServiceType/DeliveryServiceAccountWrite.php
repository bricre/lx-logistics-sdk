<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceType;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceType.
 */
class DeliveryServiceAccountWrite extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;
}
