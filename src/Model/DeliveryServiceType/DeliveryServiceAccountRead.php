<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceType;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceType.
 */
class DeliveryServiceAccountRead extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $type = null;
}
