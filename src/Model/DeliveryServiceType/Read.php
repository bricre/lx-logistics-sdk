<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceType;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceType.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $accountConfigFields = '{}';

    /**
     * @var string
     */
    public $serviceConfigField = '{}';

    /**
     * @var string
     */
    public $type = null;
}
