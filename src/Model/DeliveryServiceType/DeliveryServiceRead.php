<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceType;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceType.
 */
class DeliveryServiceRead extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $type = null;
}
