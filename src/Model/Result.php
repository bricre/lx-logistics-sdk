<?php

namespace LogisticsX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel;

class Result extends AbstractModel
{
    /**
     * @var bool
     */
    public $passed = null;

    /**
     * @var string
     */
    public $message = null;
}
