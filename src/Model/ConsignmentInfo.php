<?php

namespace LogisticsX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInfo extends AbstractModel
{
    /**
     * @var int
     */
    public $deliveryServiceId = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $postcode = null;

    /**
     * @var string
     */
    public $city = null;

    /**
     * @var string
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string
     */
    public $addressLine2 = null;

    /**
     * @var string
     */
    public $addressLine3 = null;

    /**
     * @var string
     */
    public $email = null;

    /**
     * @var string
     */
    public $vat = null;

    /**
     * @var string
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $clientReference = null;

    /**
     * @var string
     */
    public $additionalReference = null;

    /**
     * @var int
     */
    public $numberOfPallets = null;

    /**
     * @var object[]
     */
    public $product_special_attributes = null;

    /**
     * @var object[]
     */
    public $consignmentCartons = null;
}
