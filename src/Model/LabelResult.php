<?php

namespace LogisticsX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel;

class LabelResult extends AbstractModel
{
    /**
     * @var bool
     */
    public $passed = null;

    /**
     * @var string[]
     */
    public $shipmentInfo = null;

    /**
     * @var string
     */
    public $label = null;

    /**
     * @var string
     */
    public $labelType = null;

    /**
     * @var string
     */
    public $message = null;
}
