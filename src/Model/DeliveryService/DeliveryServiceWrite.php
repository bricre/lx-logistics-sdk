<?php

namespace LogisticsX\Logistics\Model\DeliveryService;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryService.
 */
class DeliveryServiceWrite extends AbstractModel
{
    /**
     * @var string
     */
    public $config = null;

    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $status = null;

    public $deliveryServiceType = null;

    public $deliveryServiceAccount = null;
}
