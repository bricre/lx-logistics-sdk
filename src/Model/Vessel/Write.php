<?php

namespace LogisticsX\Logistics\Model\Vessel;

use OpenAPI\Runtime\AbstractModel;

/**
 * Vessel.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    public $provider = null;
}
