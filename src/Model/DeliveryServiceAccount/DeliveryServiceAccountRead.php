<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceAccount;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceAccount.
 */
class DeliveryServiceAccountRead extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string
     */
    public $config = null;

    /**
     * @var string|null
     */
    public $status = null;

    public $deliveryServiceType = null;
}
