<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceAccount;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceAccount.
 */
class DeliveryServiceAccountWrite extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string
     */
    public $config = null;

    /**
     * @var string|null
     */
    public $status = null;

    public $deliveryServiceType = null;
}
