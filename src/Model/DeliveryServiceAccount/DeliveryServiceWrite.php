<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceAccount;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceAccount.
 */
class DeliveryServiceWrite extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;
}
