<?php

namespace LogisticsX\Logistics\Model\DeliveryServiceAccount;

use OpenAPI\Runtime\AbstractModel;

/**
 * DeliveryServiceAccount.
 */
class DeliveryServiceRead extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $status = null;
}
