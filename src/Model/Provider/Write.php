<?php

namespace LogisticsX\Logistics\Model\Provider;

use OpenAPI\Runtime\AbstractModel;

/**
 * Provider.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $shippingType = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $status = null;
}
