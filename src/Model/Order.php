<?php

namespace LogisticsX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel;

class Order extends AbstractModel
{
    /**
     * @var int
     */
    public $deliveryServiceId = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $postCode = null;

    /**
     * @var string
     */
    public $city = null;

    /**
     * @var string
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string
     */
    public $addressLine2 = null;

    /**
     * @var string
     */
    public $addressLine3 = null;

    /**
     * @var string
     */
    public $email = null;

    /**
     * @var int[]
     */
    public $consignmentCartons = null;
}
