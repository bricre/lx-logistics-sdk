<?php

namespace LogisticsX\Logistics\Model\Location;

use OpenAPI\Runtime\AbstractModel;

/**
 * Location.
 */
class Write extends AbstractModel
{
    /**
     * @var string|null
     */
    public $code = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $address = null;
}
