<?php

namespace LogisticsX\Logistics\Model\Location;

use OpenAPI\Runtime\AbstractModel;

/**
 * Location.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $code = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $address = null;
}
