<?php

namespace LogisticsX\Logistics;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'cancelLabel' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Result',
        ],
        'checkLimitations' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Result',
        ],
        'getDeliveryServiceAccountCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceAccount\\DeliveryServiceAccountRead[]',
        ],
        'postDeliveryServiceAccountCollection' => [
            '201.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceAccount\\DeliveryServiceAccountRead',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsDeliveryServiceAccountCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceAccount\\Statistics[]',
        ],
        'getDeliveryServiceAccountItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceAccount\\DeliveryServiceAccountRead',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putDeliveryServiceAccountItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceAccount\\DeliveryServiceAccountRead',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteDeliveryServiceAccountItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getDeliveryServiceTypeCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceType\\Read[]',
        ],
        'getDeliveryServiceTypeItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryServiceType\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getDeliveryServiceCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryService\\DeliveryServiceRead[]',
        ],
        'postDeliveryServiceCollection' => [
            '201.' => 'LogisticsX\\Logistics\\Model\\DeliveryService\\DeliveryServiceRead',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsDeliveryServiceCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryService\\Statistics[]',
        ],
        'getDeliveryServiceItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryService\\DeliveryServiceRead',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putDeliveryServiceItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\DeliveryService\\DeliveryServiceRead',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteDeliveryServiceItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLabel' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\LabelResult',
        ],
        'getLocationCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Location\\Read[]',
        ],
        'postLocationCollection' => [
            '201.' => 'LogisticsX\\Logistics\\Model\\Location\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Location\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putLocationItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Location\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteLocationItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Provider\\Read[]',
        ],
        'postProviderCollection' => [
            '201.' => 'LogisticsX\\Logistics\\Model\\Provider\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsProviderCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Provider\\Statistics[]',
        ],
        'getProviderItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Provider\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putProviderItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Provider\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteProviderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getVesselCollection' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Vessel\\Read[]',
        ],
        'postVesselCollection' => [
            '201.' => 'LogisticsX\\Logistics\\Model\\Vessel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getVesselItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Vessel\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putVesselItem' => [
            '200.' => 'LogisticsX\\Logistics\\Model\\Vessel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteVesselItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
